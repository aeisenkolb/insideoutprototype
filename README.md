# SETUP
Before starting the prototype, ensure that you have an ssh tunnel with the local port 8282 to the demo2 server open:

    ssh -L 8282:demo2.mico-project.eu:8080 -p 22102 yourID@demo2.mico-project.eu   

# Run the Prototype
Start the application

         mvn spring-boot:run
         
OR use a Spring-Boot Run Configuration in your IDE