package org.mico.project.uop.demo;

import com.github.anno4j.Anno4j;
import com.github.anno4j.model.Annotation;
import com.github.anno4j.model.impl.selector.FragmentSelector;
import com.github.anno4j.model.impl.target.SpecificResource;
import com.github.anno4j.querying.Comparison;
import com.github.anno4j.querying.QueryService;
import org.apache.marmotta.ldpath.parser.ParseException;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.config.RepositoryConfigException;
import org.openrdf.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableAutoConfiguration
public class Application {

    final static private Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        LOG.info("MICO Prototype Server started!");

        try {
            // Running the prototype for insideout
            List<Annotation> annotationList = queryDemoServer();
            List<FragmentSelector> temporalFragment = getTemporalFragment(annotationList.get(0));

            System.out.println("Faceannotation = " + annotationList.get(0).toString());
            System.out.println("Spatial = " + ((SpecificResource) annotationList.get(0).getTarget()).getSelector());
            System.out.println("Temporal = " + temporalFragment.get(0).toString());

//            LOG.info("List of queried annotation objects: \r\n");
//
//            LOG.info(annotationList.size() + " Annotation objects found");
//            for (Annotation an : annotationList) {
//                LOG.info("Current annotation object: \r\n {}", an.toString());
//            }
        } catch (RepositoryConfigException e) {
            LOG.error("Error while adding the repository to Anno4j " + e);
        } catch (RepositoryException e) {
            LOG.error("Error while adding the repository to Anno4j " + e);
        } catch (QueryEvaluationException e) {
            LOG.error("Error while querying the repository: " + e);
        } catch (MalformedQueryException e) {
            LOG.error("Generated query was maleformed " + e);
        } catch (ParseException e) {
            LOG.error("Error while parsing created query " + e);
        }
    }

    private static List<Annotation> queryDemoServer() throws RepositoryConfigException, RepositoryException, ParseException, MalformedQueryException, QueryEvaluationException {
        Anno4j anno4j = Anno4j.getInstance();
        // Configuring the repository for Anno4j, but using the default Anno4j IDGenerator
        SPARQLRepository repository = new SPARQLRepository("http://demo2.mico-project.eu:8080/marmotta/sparql/select", "http://demo2.mico-project.eu:8080/marmotta/sparql/update");
        repository.setUsernameAndPassword("your", "credentials");
        repository.initialize();
        anno4j.setRepository(repository);

        String contentPartId = "http://demo2.mico-project.eu:8080/marmotta/3088856a-7094-4c6c-b7cb-591f230ce92a";
        String ldPath = "^mico:hasContent/^mico:hasContentPart/mico:hasContentPart";

        // Getting the QueryService to query for Annotation objects, setting the mico namespace and adding a criteria.
        QueryService queryService = anno4j
                .createQueryService(Annotation.class)
                .addPrefix("mico", "http://www.mico-project.eu/ns/platform/1.0/schema#")
                .setAnnotationCriteria(ldPath, contentPartId)
                .setBodyCriteria("[is-a mico:FaceDetectionBody]");


        // executing the query
        return queryService.execute();
    }

    private static List<FragmentSelector> getTemporalFragment(Annotation faceAnnotation) throws RepositoryException, QueryEvaluationException, MalformedQueryException, ParseException {
        Anno4j anno4j = Anno4j.getInstance();

        List<Annotation> anShot = anno4j
                .createQueryService(Annotation.class)
                .addPrefix("mico", "http://www.mico-project.eu/ns/platform/1.0/schema#")
                .addPrefix("dct", "http://purl.org/dc/terms/")
                .setAnnotationCriteria("oa:hasBody[is-a mico:TVSShotBoundaryFrameBody] | oa:hasBody[is-a mico:TVSKeyFrameBody]")
                .setAnnotationCriteria("^mico:hasContent/^dct:source/^oa:hasSource/^oa:hasTarget", faceAnnotation.getResource().toString())
                .execute();

        List<FragmentSelector> results = new ArrayList<>();
        for(Annotation anno : anShot) {
            results.add((FragmentSelector) ((SpecificResource) anno.getTarget()).getSelector());
        }

        return results;
    }
}
